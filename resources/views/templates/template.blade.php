<!DOCTYPE html>
<html>
<head>
	<title>@yield("title")</title>

	{{-- bootswatch --}}
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cerulean/bootstrap.css">
</head>
<body>
	{{-- Navbar --}}
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  	<a class="navbar-brand" href="#">Navbar</a>
	  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
	   			 <span class="navbar-toggler-icon"></span>
	 		 </button>

	<div class="collapse navbar-collapse" id="navbarColor02">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Features</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Pricing</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">About</a>
	      </li>
	    </ul>
	  </div>
	</nav>

	@yield("content")

	<footer class="footer bg-dark">
		<div class="container">
			<p class="text-center text-white">2020 &copy; All Rights Reserved.</p>
		</div>
	</footer>




</body>
</html>